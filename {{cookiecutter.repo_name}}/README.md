# {{cookiecutter.project_name}}

## Requirements

- python3
- ansible
- pipenv

## Starting out

```bash
pipenv install --dev
```

## Credits

This package was created with Cookiecutter and the [mkuurstra/cookiecutter-ansible](https://gitlab.com/mkuurstra/cookiecutter-ansible) project template.
